From 7d1444d9563575ec3346620f12788799080db8c5 Mon Sep 17 00:00:00 2001
Message-Id: <7d1444d9563575ec3346620f12788799080db8c5.1683109787.git.aclaudi@redhat.com>
In-Reply-To: <d60a7ac3c0f6aa2a933f48a69ab31e3637f6906c.1683109787.git.aclaudi@redhat.com>
References: <d60a7ac3c0f6aa2a933f48a69ab31e3637f6906c.1683109787.git.aclaudi@redhat.com>
From: Andrea Claudi <aclaudi@redhat.com>
Date: Wed, 3 May 2023 11:19:24 +0200
Subject: [PATCH] macvlan: Add bclim parameter

Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=2186945
Upstream Status: iproute2-next.git commit e8a3fb47

commit e8a3fb470b4e96aa35a2731c7cc175b946c0a62d
Author: Herbert Xu <herbert@gondor.apana.org.au>
Date:   Thu Mar 30 11:07:25 2023 +0800

    macvlan: Add bclim parameter

    This patch adds support for setting the broadcast queueing threshold
    on macvlan devices.  This controls which multicast packets will be
    processed in a workqueue instead of inline.

    Signed-off-by: Herbert Xu <herbert@gondor.apana.org.au>

     ip/iplink_macvlan.c          |   26 ++++++++++++++++++++++++--
     man/man8/ip-link.8.in        |   18 ++++++++++++++++++
     3 files changed, 43 insertions(+), 2 deletions(-)

    Signed-off-by: David Ahern <dsahern@kernel.org>
---
 ip/iplink_macvlan.c   | 26 ++++++++++++++++++++++++--
 man/man8/ip-link.8.in | 18 ++++++++++++++++++
 2 files changed, 42 insertions(+), 2 deletions(-)

diff --git a/ip/iplink_macvlan.c b/ip/iplink_macvlan.c
index 0f13637d..6bdc76d1 100644
--- a/ip/iplink_macvlan.c
+++ b/ip/iplink_macvlan.c
@@ -26,13 +26,14 @@
 static void print_explain(struct link_util *lu, FILE *f)
 {
 	fprintf(f,
-		"Usage: ... %s mode MODE [flag MODE_FLAG] MODE_OPTS [bcqueuelen BC_QUEUE_LEN]\n"
+		"Usage: ... %s mode MODE [flag MODE_FLAG] MODE_OPTS [bcqueuelen BC_QUEUE_LEN] [bclim BCLIM]\n"
 		"\n"
 		"MODE: private | vepa | bridge | passthru | source\n"
 		"MODE_FLAG: null | nopromisc | nodst\n"
 		"MODE_OPTS: for mode \"source\":\n"
 		"\tmacaddr { { add | del } <macaddr> | set [ <macaddr> [ <macaddr>  ... ] ] | flush }\n"
-		"BC_QUEUE_LEN: Length of the rx queue for broadcast/multicast: [0-4294967295]\n",
+		"BC_QUEUE_LEN: Length of the rx queue for broadcast/multicast: [0-4294967295]\n"
+		"BCLIM: Threshold for broadcast queueing: 32-bit integer\n",
 		lu->id
 	);
 }
@@ -67,6 +68,12 @@ static int bc_queue_len_arg(const char *arg)
 	return -1;
 }
 
+static int bclim_arg(const char *arg)
+{
+	fprintf(stderr, "Error: illegal value for \"bclim\": \"%s\"\n", arg);
+	return -1;
+}
+
 static int macvlan_parse_opt(struct link_util *lu, int argc, char **argv,
 			  struct nlmsghdr *n)
 {
@@ -168,6 +175,15 @@ static int macvlan_parse_opt(struct link_util *lu, int argc, char **argv,
 				return bc_queue_len_arg(*argv);
 			}
 			addattr32(n, 1024, IFLA_MACVLAN_BC_QUEUE_LEN, bc_queue_len);
+		} else if (!strcmp(*argv, "bclim")) {
+			__s32 bclim;
+			NEXT_ARG();
+
+			if (get_s32(&bclim, *argv, 0)) {
+				return bclim_arg(*argv);
+			}
+			addattr_l(n, 1024, IFLA_MACVLAN_BC_CUTOFF,
+				  &bclim, sizeof(bclim));
 		} else if (matches(*argv, "help") == 0) {
 			explain(lu);
 			return -1;
@@ -245,6 +261,12 @@ static void macvlan_print_opt(struct link_util *lu, FILE *f, struct rtattr *tb[]
 		print_luint(PRINT_ANY, "usedbcqueuelen", "usedbcqueuelen %lu ", bc_queue_len);
 	}
 
+	if (tb[IFLA_MACVLAN_BC_CUTOFF] &&
+		RTA_PAYLOAD(tb[IFLA_MACVLAN_BC_CUTOFF]) >= sizeof(__s32)) {
+		__s32 bclim = rta_getattr_s32(tb[IFLA_MACVLAN_BC_CUTOFF]);
+		print_int(PRINT_ANY, "bclim", "bclim %d ", bclim);
+	}
+
 	/* in source mode, there are more options to print */
 
 	if (mode != MACVLAN_MODE_SOURCE)
diff --git a/man/man8/ip-link.8.in b/man/man8/ip-link.8.in
index eeddf493..62aebabd 100644
--- a/man/man8/ip-link.8.in
+++ b/man/man8/ip-link.8.in
@@ -1455,6 +1455,7 @@ the following additional arguments are supported:
 .BR mode " { " private " | " vepa " | " bridge " | " passthru
 .RB " [ " nopromisc " ] | " source " [ " nodst " ] } "
 .RB " [ " bcqueuelen " { " LENGTH " } ] "
+.RB " [ " bclim " " LIMIT " ] "
 
 .in +8
 .sp
@@ -1513,6 +1514,13 @@ will be the maximum length that any macvlan interface has requested.
 When listing device parameters both the bcqueuelen parameter
 as well as the actual used bcqueuelen are listed to better help
 the user understand the setting.
+
+.BR bclim " " LIMIT
+- Set the threshold for broadcast queueing.
+.BR LIMIT " must be a 32-bit integer."
+Setting this to -1 disables broadcast queueing altogether.  Otherwise
+a multicast address will be queued as broadcast if the number of devices
+using it is greater than the given value.
 .in -8
 
 .TP
@@ -2675,6 +2683,9 @@ Update the broadcast/multicast queue length.
 [
 .BI bcqueuelen "  LENGTH  "
 ]
+[
+.BI bclim " LIMIT "
+]
 
 .in +8
 .BI bcqueuelen " LENGTH "
@@ -2688,6 +2699,13 @@ will be the maximum length that any macvlan interface has requested.
 When listing device parameters both the bcqueuelen parameter
 as well as the actual used bcqueuelen are listed to better help
 the user understand the setting.
+
+.BI bclim " LIMIT "
+- Set the threshold for broadcast queueing.
+.IR LIMIT " must be a 32-bit integer."
+Setting this to -1 disables broadcast queueing altogether.  Otherwise
+a multicast address will be queued as broadcast if the number of devices
+using it is greater than the given value.
 .in -8
 
 .TP
-- 
2.40.1

